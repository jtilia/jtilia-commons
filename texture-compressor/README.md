## Texture Compressor

BC1/BC3/BC4/BC5/BC7 texture compressor based on:
- https://github.com/nothings/stb/blob/master/stb_dxt.h 
- https://github.com/richgel999/bc7enc16. 