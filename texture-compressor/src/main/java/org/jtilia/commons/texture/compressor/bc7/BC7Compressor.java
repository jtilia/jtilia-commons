/*
 * This code is based on https://github.com/richgel999/bc7enc16
 * Copyright(c) 2018 Richard Geldreich, Jr.
 * Java port copyright (c) 2018 Piotr Piastucki piotr@jtilia.org
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files(the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and / or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions :
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.jtilia.commons.texture.compressor.bc7;

import static org.jtilia.commons.texture.compressor.bc7.Utils.clampf;
import static org.jtilia.commons.texture.compressor.bc7.Utils.clampi;
import static org.jtilia.commons.texture.compressor.bc7.Utils.squaref;
import static org.jtilia.commons.texture.compressor.bc7.Utils.squarei;

import java.util.Arrays;

public class BC7Compressor {

    // private static final int MAX_BLOCK_SIZE = 16;
    static final int MAX_PARTITIONS1 = 64;

    // Various BC7 tables
    private static final int g_bc7_weights3[] = { 0, 9, 18, 27, 37, 46, 55, 64 };
    private static final int g_bc7_weights4[] = { 0, 4, 9, 13, 17, 21, 26, 30, 34, 38, 43, 47, 51, 55, 60, 64 };
    // Precomputed weight constants used during least fit determination. For each entry in g_bc7_weights[]: w * w, (1.0f - w) * w,
    // (1.0f - w) * (1.0f - w), w
    private static final Vec4f g_bc7_weights3x[] = { new Vec4f(0.000000f, 0.000000f, 1.000000f, 0.000000f),
            new Vec4f(0.019775f, 0.120850f, 0.738525f, 0.140625f), new Vec4f(0.079102f, 0.202148f, 0.516602f, 0.281250f),
            new Vec4f(0.177979f, 0.243896f, 0.334229f, 0.421875f), new Vec4f(0.334229f, 0.243896f, 0.177979f, 0.578125f),
            new Vec4f(0.516602f, 0.202148f, 0.079102f, 0.718750f), new Vec4f(0.738525f, 0.120850f, 0.019775f, 0.859375f),
            new Vec4f(1.000000f, 0.000000f, 0.000000f, 1.000000f) };
    private static final Vec4f g_bc7_weights4x[] = { new Vec4f(0.000000f, 0.000000f, 1.000000f, 0.000000f),
            new Vec4f(0.003906f, 0.058594f, 0.878906f, 0.062500f), new Vec4f(0.019775f, 0.120850f, 0.738525f, 0.140625f),
            new Vec4f(0.041260f, 0.161865f, 0.635010f, 0.203125f), new Vec4f(0.070557f, 0.195068f, 0.539307f, 0.265625f),
            new Vec4f(0.107666f, 0.220459f, 0.451416f, 0.328125f), new Vec4f(0.165039f, 0.241211f, 0.352539f, 0.406250f),
            new Vec4f(0.219727f, 0.249023f, 0.282227f, 0.468750f), new Vec4f(0.282227f, 0.249023f, 0.219727f, 0.531250f),
            new Vec4f(0.352539f, 0.241211f, 0.165039f, 0.593750f), new Vec4f(0.451416f, 0.220459f, 0.107666f, 0.671875f),
            new Vec4f(0.539307f, 0.195068f, 0.070557f, 0.734375f), new Vec4f(0.635010f, 0.161865f, 0.041260f, 0.796875f),
            new Vec4f(0.738525f, 0.120850f, 0.019775f, 0.859375f), new Vec4f(0.878906f, 0.058594f, 0.003906f, 0.937500f),
            new Vec4f(1.000000f, 0.000000f, 0.000000f, 1.000000f) };
    private static final byte g_bc7_partition1[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    private static final byte g_bc7_partition2[] = { 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0,
            0, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1,
            0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0,
            0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1,
            1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1,
            1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0,
            0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0,
            0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1,
            0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0,
            0, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0,
            1, 0, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1,
            1, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 0, 1,
            0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0,
            1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0,
            1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0,
            0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0,
            1, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1,
            1, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1,
            1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1,
            1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1,
            1, 1, 0, 1, 1, 1 };
    // Partition order sorted by usage frequency across a large test corpus. Pattern 34 (checkerboard) must appear in
    // slot 34.
    // Using a sorted order allows the user to decrease the # of partitions to scan with minimal loss in quality.
    private static final int s_sorted_partition_order[] = new int[] { 1 - 1, 14 - 1, 2 - 1, 3 - 1, 16 - 1, 15 - 1,
            11 - 1, 17 - 1, 4 - 1, 24 - 1, 27 - 1, 7 - 1, 8 - 1, 22 - 1, 20 - 1, 30 - 1, 9 - 1, 5 - 1, 10 - 1, 21 - 1,
            6 - 1, 32 - 1, 23 - 1, 18 - 1, 19 - 1, 12 - 1, 13 - 1, 31 - 1, 25 - 1, 26 - 1, 29 - 1, 28 - 1, 33 - 1,
            34 - 1, 35 - 1, 46 - 1, 47 - 1, 52 - 1, 50 - 1, 51 - 1, 49 - 1, 39 - 1, 40 - 1, 38 - 1, 54 - 1, 53 - 1,
            55 - 1, 37 - 1, 58 - 1, 59 - 1, 56 - 1, 42 - 1, 41 - 1, 43 - 1, 44 - 1, 60 - 1, 45 - 1, 57 - 1, 48 - 1,
            36 - 1, 61 - 1, 64 - 1, 63 - 1, 62 - 1 };

    // This table contains bitmasks indicating which "key" partitions must be best ranked before this partition is worth
    // evaluating.
    // We first rank the best/most used 14 partitions (sorted by usefulness), record the best one found as the key
    // partition, then use
    // that to control the other partitions to evaluate. The quality loss is ~.08 dB RGB PSNR, the perf gain is up to
    // ~11% (at uber level 0).
    private static final int UINT32_MAX = Integer.MAX_VALUE;
    private static final int g_partition_predictors[] = new int[] { UINT32_MAX, UINT32_MAX, UINT32_MAX, UINT32_MAX,
            UINT32_MAX, (1 << 1) | (1 << 2) | (1 << 8), (1 << 1) | (1 << 3) | (1 << 7), UINT32_MAX, UINT32_MAX,
            (1 << 2) | (1 << 8) | (1 << 16), (1 << 7) | (1 << 3) | (1 << 15), UINT32_MAX,
            (1 << 8) | (1 << 14) | (1 << 16), (1 << 7) | (1 << 14) | (1 << 15), UINT32_MAX, UINT32_MAX, UINT32_MAX,
            UINT32_MAX, (1 << 14) | (1 << 15), (1 << 16) | (1 << 22) | (1 << 14), (1 << 17) | (1 << 24) | (1 << 14),
            (1 << 2) | (1 << 14) | (1 << 15) | (1 << 1), UINT32_MAX,
            (1 << 1) | (1 << 3) | (1 << 14) | (1 << 16) | (1 << 22), UINT32_MAX,
            (1 << 1) | (1 << 2) | (1 << 15) | (1 << 17) | (1 << 24), (1 << 1) | (1 << 3) | (1 << 22), UINT32_MAX,
            UINT32_MAX, UINT32_MAX, (1 << 14) | (1 << 15) | (1 << 16) | (1 << 17), UINT32_MAX, UINT32_MAX,
            (1 << 1) | (1 << 2) | (1 << 3) | (1 << 27) | (1 << 4) | (1 << 24),
            (1 << 14) | (1 << 15) | (1 << 16) | (1 << 11) | (1 << 17) | (1 << 27) };

    private static final byte g_bc7_table_anchor_index_second_subset[] = { 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
            15, 2, 8, 2, 2, 8, 8, 15, 2, 8, 2, 2, 8, 8, 2, 2, 15, 15, 6, 8, 2, 8, 15, 15, 2, 8, 2, 2, 2, 15, 15, 6, 6, 2, 6, 8, 15, 15, 2,
            2, 15, 15, 15, 15, 15, 2, 2, 15 };
    private static final byte g_bc7_num_subsets[] = { 3, 2, 3, 2, 1, 1, 1, 2 };
    private static final byte g_bc7_partition_bits[] = { 4, 6, 6, 6, 0, 0, 0, 6 };
    private static final byte g_bc7_color_index_bitcount[] = { 3, 3, 2, 2, 2, 2, 4, 2 };

    private static final byte get_bc7_color_index_size(int mode, int index_selection_bit) {
        return (byte) (g_bc7_color_index_bitcount[mode] + index_selection_bit);
    }

    // private static final byte g_bc7_mode_has_p_bits[] = { 1, 1, 0, 1, 0, 0, 1, 1 };
    private static final byte g_bc7_mode_has_shared_p_bits[] = { 0, 1, 0, 0, 0, 0, 0, 0 };
    private static final byte g_bc7_color_precision_table[] = { 4, 6, 5, 7, 5, 7, 7, 5 };
    private static final byte g_bc7_alpha_precision_table[] = { 0, 0, 0, 0, 6, 8, 7, 5 };

    private class EndpointErr {
        int m_error;
        int m_lo;
        int m_hi;
    }

    private static final EndpointErr[][] g_bc7_mode_1_optimal_endpoints = new EndpointErr[256][2]; // [c][pbit]
    private static final byte BC7ENC16_MODE_1_OPTIMAL_INDEX = 2;

    // Initialize the lookup table used for optimal single color compression in mode 1. Must be called before encoding.
    public BC7Compressor() {
        for (int c = 0; c < 256; c++) {
            for (int lp = 0; lp < 2; lp++) {
                EndpointErr best = new EndpointErr();
                best.m_error = Integer.MAX_VALUE;
                for (int l = 0; l < 64; l++) {
                    int low = ((l << 1) | lp) << 1;
                    low |= (low >> 7);
                    for (int h = 0; h < 64; h++) {
                        int high = ((h << 1) | lp) << 1;
                        high |= (high >> 7);
                        int k = (low * (64 - g_bc7_weights3[BC7ENC16_MODE_1_OPTIMAL_INDEX])
                                + high * g_bc7_weights3[BC7ENC16_MODE_1_OPTIMAL_INDEX] + 32) >> 6;
                        int err = (k - c) * (k - c);
                        if (err < best.m_error) {
                            best.m_error = err;
                            best.m_lo = l;
                            best.m_hi = h;
                        }
                    } // h
                } // l
                g_bc7_mode_1_optimal_endpoints[c][lp] = best;
            } // lp
        } // c
    }

    private void compute_least_squares_endpoints_rgba(int N, byte[] pSelectors, Vec4f[] pSelector_weights, Vec4f pXl, Vec4f pXh,
            ColorQuad[] pColors) {
        // Least squares using normal equations: http://www.cs.cornell.edu/~bindel/class/cs3220-s12/notes/lec10.pdf
        // I did this in matrix form first, expanded out all the ops, then optimized it a bit.
        float z00 = 0.0f, z01 = 0.0f, z10 = 0.0f, z11 = 0.0f;
        float q00_r = 0.0f, q10_r = 0.0f, t_r = 0.0f;
        float q00_g = 0.0f, q10_g = 0.0f, t_g = 0.0f;
        float q00_b = 0.0f, q10_b = 0.0f, t_b = 0.0f;
        float q00_a = 0.0f, q10_a = 0.0f, t_a = 0.0f;
        for (int i = 0; i < N; i++) {
            int sel = pSelectors[i];
            z00 += pSelector_weights[sel].m_c[0];
            z10 += pSelector_weights[sel].m_c[1];
            z11 += pSelector_weights[sel].m_c[2];
            float w = pSelector_weights[sel].m_c[3];
            q00_r += w * pColors[i].get(0);
            t_r += pColors[i].get(0);
            q00_g += w * pColors[i].get(1);
            t_g += pColors[i].get(1);
            q00_b += w * pColors[i].get(2);
            t_b += pColors[i].get(2);
            q00_a += w * pColors[i].get(3);
            t_a += pColors[i].get(3);
        }

        q10_r = t_r - q00_r;
        q10_g = t_g - q00_g;
        q10_b = t_b - q00_b;
        q10_a = t_a - q00_a;

        z01 = z10;

        float det = z00 * z11 - z01 * z10;
        if (det != 0.0f) {
            det = 1.0f / det;
        }

        float iz00, iz01, iz10, iz11;
        iz00 = z11 * det;
        iz01 = -z01 * det;
        iz10 = -z10 * det;
        iz11 = z00 * det;

        pXl.m_c[0] = iz00 * q00_r + iz01 * q10_r;
        pXh.m_c[0] = iz10 * q00_r + iz11 * q10_r;
        pXl.m_c[1] = iz00 * q00_g + iz01 * q10_g;
        pXh.m_c[1] = iz10 * q00_g + iz11 * q10_g;
        pXl.m_c[2] = iz00 * q00_b + iz01 * q10_b;
        pXh.m_c[2] = iz10 * q00_b + iz11 * q10_b;
        pXl.m_c[3] = iz00 * q00_a + iz01 * q10_a;
        pXh.m_c[3] = iz10 * q00_a + iz11 * q10_a;
    }

    private void compute_least_squares_endpoints_rgb(int N, byte[] pSelectors, Vec4f[] pSelector_weights, Vec4f pXl, Vec4f pXh,
            ColorQuad[] pColors) {
        // Least squares using normal equations: http://www.cs.cornell.edu/~bindel/class/cs3220-s12/notes/lec10.pdf
        // I did this in matrix form first, expanded out all the ops, then optimized it a bit.
        float z00 = 0.0f, z01 = 0.0f, z10 = 0.0f, z11 = 0.0f;
        float q00_r = 0.0f, q10_r = 0.0f, t_r = 0.0f;
        float q00_g = 0.0f, q10_g = 0.0f, t_g = 0.0f;
        float q00_b = 0.0f, q10_b = 0.0f, t_b = 0.0f;
        for (int i = 0; i < N; i++) {
            int sel = pSelectors[i];
            z00 += pSelector_weights[sel].m_c[0];
            z10 += pSelector_weights[sel].m_c[1];
            z11 += pSelector_weights[sel].m_c[2];
            float w = pSelector_weights[sel].m_c[3];
            q00_r += w * pColors[i].get(0);
            t_r += pColors[i].get(0);
            q00_g += w * pColors[i].get(1);
            t_g += pColors[i].get(1);
            q00_b += w * pColors[i].get(2);
            t_b += pColors[i].get(2);
        }

        q10_r = t_r - q00_r;
        q10_g = t_g - q00_g;
        q10_b = t_b - q00_b;

        z01 = z10;

        float det = z00 * z11 - z01 * z10;
        if (det != 0.0f) {
            det = 1.0f / det;
        }

        float iz00, iz01, iz10, iz11;
        iz00 = z11 * det;
        iz01 = -z01 * det;
        iz10 = -z10 * det;
        iz11 = z00 * det;

        pXl.m_c[0] = iz00 * q00_r + iz01 * q10_r;
        pXh.m_c[0] = iz10 * q00_r + iz11 * q10_r;
        pXl.m_c[1] = iz00 * q00_g + iz01 * q10_g;
        pXh.m_c[1] = iz10 * q00_g + iz11 * q10_g;
        pXl.m_c[2] = iz00 * q00_b + iz01 * q10_b;
        pXh.m_c[2] = iz10 * q00_b + iz11 * q10_b;
        pXl.m_c[3] = 255f;
        pXh.m_c[3] = 255f;
    }

    private class ColorCellCompressorParams {
        private int m_num_pixels;
        private ColorQuad[] m_pPixels;
        private int m_num_selector_weights;
        private int[] m_pSelector_weights;
        private Vec4f[] m_pSelector_weightsx;
        private int m_comp_bits;
        private final int[] m_weights = new int[4];
        private boolean m_has_alpha;
        private boolean m_has_pbits;
        private boolean m_endpoints_share_pbit;
        private boolean m_perceptual;
    };

    private class ColorCellCompressorResults {
        private long m_best_overall_err;
        private final ColorQuad m_low_endpoint = new ColorQuad();
        private final ColorQuad m_high_endpoint = new ColorQuad();
        private final int m_pbits[] = new int[2];
        private byte[] m_pSelectors;
        private byte[] m_pSelectors_temp;

        ColorCellCompressorResults() {
        }

        ColorCellCompressorResults(ColorCellCompressorResults other) {
            m_best_overall_err = other.m_best_overall_err;
            m_low_endpoint.set(other.m_low_endpoint);
            m_high_endpoint.set(other.m_high_endpoint);
            m_pbits[0] = other.m_pbits[0];
            m_pbits[1] = other.m_pbits[1];
            m_pSelectors = other.m_pSelectors;
            m_pSelectors_temp = other.m_pSelectors_temp;
        }
    }

    private ColorQuad scale_color(ColorQuad pC, ColorCellCompressorParams pParams) {
        ColorQuad results = new ColorQuad();

        int n = pParams.m_comp_bits + (pParams.m_has_pbits ? 1 : 0);
        assert ((n >= 4) && (n <= 8));

        for (int i = 0; i < 4; i++) {
            int v = pC.get(i) << (8 - n);
            v |= (v >> n);
            assert (v <= 255);
            results.set(i, v);
        }

        return results;
    }

    private long compute_color_distance_rgb(ColorQuad pE1, ColorQuad pE2, boolean perceptual, int[] weights) {
        int dr, dg, db;

        if (perceptual) {
            int l1 = pE1.get(0) * 109 + pE1.get(1) * 366 + pE1.get(2) * 37;
            int cr1 = (pE1.get(0) << 9) - l1;
            int cb1 = (pE1.get(2) << 9) - l1;
            int l2 = pE2.get(0) * 109 + pE2.get(1) * 366 + pE2.get(2) * 37;
            int cr2 = (pE2.get(0) << 9) - l2;
            int cb2 = (pE2.get(2) << 9) - l2;
            dr = (l1 - l2) >> 8;
            dg = (cr1 - cr2) >> 8;
            db = (cb1 - cb2) >> 8;
        } else {
            dr = pE1.get(0) - pE2.get(0);
            dg = pE1.get(1) - pE2.get(1);
            db = pE1.get(2) - pE2.get(2);
        }

        return weights[0] * (dr * dr) + weights[1] * (dg * dg) + weights[2] * (db * db);
    }

    private long compute_color_distance_rgba(ColorQuad pE1, ColorQuad pE2, boolean perceptual, int[] weights) {
        int da = pE1.get(3) - pE2.get(3);
        return compute_color_distance_rgb(pE1, pE2, perceptual, weights) + (weights[3] * (da * da));
    }

    private ColorCellCompressorResults pack_mode1_to_one_color(ColorCellCompressorParams pParams, ColorCellCompressorResults pResults,
            int r, int g, int b, byte[] pSelectors) {
        int best_err = Integer.MAX_VALUE;
        int best_p = 0;

        for (int p = 0; p < 2; p++) {
            int err = g_bc7_mode_1_optimal_endpoints[r][p].m_error + g_bc7_mode_1_optimal_endpoints[g][p].m_error
                    + g_bc7_mode_1_optimal_endpoints[b][p].m_error;
            if (err < best_err) {
                best_err = err;
                best_p = p;
            }
        }

        EndpointErr pEr = g_bc7_mode_1_optimal_endpoints[r][best_p];
        EndpointErr pEg = g_bc7_mode_1_optimal_endpoints[g][best_p];
        EndpointErr pEb = g_bc7_mode_1_optimal_endpoints[b][best_p];

        pResults.m_low_endpoint.set(pEr.m_lo, pEg.m_lo, pEb.m_lo, 0);
        pResults.m_high_endpoint.set(pEr.m_hi, pEg.m_hi, pEb.m_hi, 0);
        pResults.m_pbits[0] = best_p;
        pResults.m_pbits[1] = 0;

        Arrays.fill(pSelectors, 0, pParams.m_num_pixels, BC7ENC16_MODE_1_OPTIMAL_INDEX);

        ColorQuad p = new ColorQuad();
        for (int i = 0; i < 3; i++) {
            int low = ((pResults.m_low_endpoint.get(i) << 1) | pResults.m_pbits[0]) << 1;
            low |= (low >> 7);

            int high = ((pResults.m_high_endpoint.get(i) << 1) | pResults.m_pbits[0]) << 1;
            high |= (high >> 7);

            p.set(i, ((low * (64 - g_bc7_weights3[BC7ENC16_MODE_1_OPTIMAL_INDEX]) + high * g_bc7_weights3[BC7ENC16_MODE_1_OPTIMAL_INDEX]
                    + 32) >> 6));
        }
        p.set(3, 255);

        long total_err = 0;
        for (int i = 0; i < pParams.m_num_pixels; i++) {
            total_err += compute_color_distance_rgb(p, pParams.m_pPixels[i], pParams.m_perceptual, pParams.m_weights);
        }
        pResults.m_best_overall_err = total_err;

        return pResults;
    }

    long evaluate_solution(ColorQuad pLow, ColorQuad pHigh, int[] pbits, ColorCellCompressorParams pParams,
            ColorCellCompressorResults pResults) {
        ColorQuad quantMinColor = new ColorQuad(pLow);
        ColorQuad quantMaxColor = new ColorQuad(pHigh);

        if (pParams.m_has_pbits) {
            int minPBit, maxPBit;

            if (pParams.m_endpoints_share_pbit)
                maxPBit = minPBit = pbits[0];
            else {
                minPBit = pbits[0];
                maxPBit = pbits[1];
            }

            quantMinColor.set(0, ((pLow.get(0) << 1) | minPBit));
            quantMinColor.set(1, ((pLow.get(1) << 1) | minPBit));
            quantMinColor.set(2, ((pLow.get(2) << 1) | minPBit));
            quantMinColor.set(3, ((pLow.get(3) << 1) | minPBit));

            quantMaxColor.set(0, ((pHigh.get(0) << 1) | maxPBit));
            quantMaxColor.set(1, ((pHigh.get(1) << 1) | maxPBit));
            quantMaxColor.set(2, ((pHigh.get(2) << 1) | maxPBit));
            quantMaxColor.set(3, ((pHigh.get(3) << 1) | maxPBit));
        }

        ColorQuad actualMinColor = scale_color(quantMinColor, pParams);
        ColorQuad actualMaxColor = scale_color(quantMaxColor, pParams);

        int N = pParams.m_num_selector_weights;

        ColorQuad[] weightedColors = new ColorQuad[16];
        weightedColors[0] = actualMinColor;
        weightedColors[N - 1] = actualMaxColor;

        int nc = pParams.m_has_alpha ? 4 : 3;
        for (int i = 1; i < (N - 1); i++) {
            weightedColors[i] = new ColorQuad();
            for (int j = 0; j < nc; j++) {
                weightedColors[i].set(j, ((actualMinColor.get(j) * (64 - pParams.m_pSelector_weights[i])
                        + actualMaxColor.get(j) * pParams.m_pSelector_weights[i] + 32) >> 6));
            }
        }
        int lr = actualMinColor.get(0);
        int lg = actualMinColor.get(1);
        int lb = actualMinColor.get(2);
        int dr = actualMaxColor.get(0) - lr;
        int dg = actualMaxColor.get(1) - lg;
        int db = actualMaxColor.get(2) - lb;

        long total_err = 0;

        if (!pParams.m_perceptual) {
            if (pParams.m_has_alpha) {
                int la = actualMinColor.get(3);
                int da = actualMaxColor.get(3) - la;

                float f = N / (squarei(dr) + squarei(dg) + squarei(db) + squarei(da) + .00000125f);

                for (int i = 0; i < pParams.m_num_pixels; i++) {
                    ColorQuad pC = pParams.m_pPixels[i];
                    int r = pC.get(0);
                    int g = pC.get(1);
                    int b = pC.get(2);
                    int a = pC.get(3);

                    int best_sel = (int) (((r - lr) * dr + (g - lg) * dg + (b - lb) * db + (a - la) * da) * f + .5f);
                    best_sel = clampi(best_sel, 1, N - 1);

                    long err0 = compute_color_distance_rgba(weightedColors[best_sel - 1], pC, false, pParams.m_weights);
                    long err1 = compute_color_distance_rgba(weightedColors[best_sel], pC, false, pParams.m_weights);

                    if (err1 > err0) {
                        err1 = err0;
                        --best_sel;
                    }
                    total_err += err1;

                    pResults.m_pSelectors_temp[i] = (byte) best_sel;
                }
            } else {
                float f = N / (squarei(dr) + squarei(dg) + squarei(db) + .00000125f);

                for (int i = 0; i < pParams.m_num_pixels; i++) {
                    ColorQuad pC = pParams.m_pPixels[i];
                    int r = pC.get(0);
                    int g = pC.get(1);
                    int b = pC.get(2);

                    int sel = (int) (((r - lr) * dr + (g - lg) * dg + (b - lb) * db) * f + .5f);
                    sel = clampi(sel, 1, N - 1);

                    long err0 = compute_color_distance_rgb(weightedColors[sel - 1], pC, false, pParams.m_weights);
                    long err1 = compute_color_distance_rgb(weightedColors[sel], pC, false, pParams.m_weights);

                    int best_sel = sel;
                    long best_err = err1;
                    if (err0 < best_err) {
                        best_err = err0;
                        best_sel = sel - 1;
                    }

                    total_err += best_err;

                    pResults.m_pSelectors_temp[i] = (byte) best_sel;
                }
            }
        } else {
            for (int i = 0; i < pParams.m_num_pixels; i++) {
                long best_err = Long.MAX_VALUE;
                int best_sel = 0;

                if (pParams.m_has_alpha) {
                    for (int j = 0; j < N; j++) {
                        long err = compute_color_distance_rgba(weightedColors[j], pParams.m_pPixels[i], true, pParams.m_weights);
                        if (err < best_err) {
                            best_err = err;
                            best_sel = j;
                        }
                    }
                } else {
                    for (int j = 0; j < N; j++) {
                        long err = compute_color_distance_rgb(weightedColors[j], pParams.m_pPixels[i], true, pParams.m_weights);
                        if (err < best_err) {
                            best_err = err;
                            best_sel = j;
                        }
                    }
                }

                total_err += best_err;

                pResults.m_pSelectors_temp[i] = (byte) best_sel;
            }
        }

        if (total_err < pResults.m_best_overall_err) {
            pResults.m_best_overall_err = total_err;

            pResults.m_low_endpoint.set(pLow);
            pResults.m_high_endpoint.set(pHigh);

            pResults.m_pbits[0] = pbits[0];
            pResults.m_pbits[1] = pbits[1];

            System.arraycopy(pResults.m_pSelectors_temp, 0, pResults.m_pSelectors, 0, pParams.m_num_pixels);
        }

        return total_err;
    }

    static void fixDegenerateEndpoints(int mode, ColorQuad pTrialMinColor, ColorQuad pTrialMaxColor, Vec4f pXl, Vec4f pXh, int iscale) {
        if (mode == 1) {
            // fix degenerate case where the input collapses to a single colorspace voxel, and we loose all freedom (test with grayscale
            // ramps)
            for (int i = 0; i < 3; i++) {
                if (pTrialMinColor.get(i) == pTrialMaxColor.get(i)) {
                    if (Math.abs(pXl.m_c[i] - pXh.m_c[i]) > 0.0f) {
                        if (pTrialMinColor.get(i) > (iscale >> 1)) {
                            if (pTrialMinColor.get(i) > 0)
                                pTrialMinColor.dec(i);
                            else if (pTrialMaxColor.get(i) < iscale)
                                pTrialMaxColor.inc(i);
                        } else {
                            if (pTrialMaxColor.get(i) < iscale)
                                pTrialMaxColor.inc(i);
                            else if (pTrialMinColor.get(i) > 0)
                                pTrialMinColor.dec(i);
                        }
                    }
                }
            }
        }
    }

    long find_optimal_solution(int mode, Vec4f xl, Vec4f xh, ColorCellCompressorParams pParams, ColorCellCompressorResults pResults) {
        xl.saturate_in_place();
        xh.saturate_in_place();

        if (pParams.m_has_pbits) {
            int iscalep = (1 << (pParams.m_comp_bits + 1)) - 1;
            float scalep = iscalep;

            int totalComps = pParams.m_has_alpha ? 4 : 3;

            int[] best_pbits = new int[2];
            ColorQuad bestMinColor = new ColorQuad(), bestMaxColor = new ColorQuad();

            if (!pParams.m_endpoints_share_pbit) {
                float best_err0 = 1e+9f;
                float best_err1 = 1e+9f;

                for (int p = 0; p < 2; p++) {
                    ColorQuad xMinColor = new ColorQuad(), xMaxColor = new ColorQuad();

                    // Notes: The pbit controls which quantization intervals are selected.
                    // total_levels=2^(comp_bits+1), where comp_bits=4 for mode 0, etc.
                    // pbit 0: v=(b*2)/(total_levels-1), pbit 1: v=(b*2+1)/(total_levels-1) where b is the component bin from
                    // [0,total_levels/2-1] and v is the [0,1] component value
                    // rearranging you get for pbit 0: b=floor(v*(total_levels-1)/2+.5)
                    // rearranging you get for pbit 1: b=floor((v*(total_levels-1)-1)/2+.5)
                    for (int c = 0; c < 4; c++) {
                        xMinColor.set(c, (clampi(((int) ((xl.m_c[c] * scalep - p) / 2.0f + .5f)) * 2 + p, p, iscalep - 1 + p)));
                        xMaxColor.set(c, (clampi(((int) ((xh.m_c[c] * scalep - p) / 2.0f + .5f)) * 2 + p, p, iscalep - 1 + p)));
                    }

                    ColorQuad scaledLow = scale_color(xMinColor, pParams);
                    ColorQuad scaledHigh = scale_color(xMaxColor, pParams);

                    float err0 = 0, err1 = 0;
                    for (int i = 0; i < totalComps; i++) {
                        err0 += squaref(scaledLow.get(i) - xl.m_c[i] * 255.0f);
                        err1 += squaref(scaledHigh.get(i) - xh.m_c[i] * 255.0f);
                    }

                    if (err0 < best_err0) {
                        best_err0 = err0;
                        best_pbits[0] = p;

                        bestMinColor.set(0, (xMinColor.get(0) >> 1));
                        bestMinColor.set(1, (xMinColor.get(1) >> 1));
                        bestMinColor.set(2, (xMinColor.get(2) >> 1));
                        bestMinColor.set(3, (xMinColor.get(3) >> 1));
                    }

                    if (err1 < best_err1) {
                        best_err1 = err1;
                        best_pbits[1] = p;

                        bestMaxColor.set(0, (xMaxColor.get(0) >> 1));
                        bestMaxColor.set(1, (xMaxColor.get(1) >> 1));
                        bestMaxColor.set(2, (xMaxColor.get(2) >> 1));
                        bestMaxColor.set(3, (xMaxColor.get(3) >> 1));
                    }
                }
            } else {
                // Endpoints share pbits
                float best_err = 1e+9f;

                for (int p = 0; p < 2; p++) {
                    ColorQuad xMinColor = new ColorQuad(), xMaxColor = new ColorQuad();
                    for (int c = 0; c < 4; c++) {
                        xMinColor.set(c, (clampi(((int) ((xl.m_c[c] * scalep - p) / 2.0f + .5f)) * 2 + p, p, iscalep - 1 + p)));
                        xMaxColor.set(c, (clampi(((int) ((xh.m_c[c] * scalep - p) / 2.0f + .5f)) * 2 + p, p, iscalep - 1 + p)));
                    }

                    ColorQuad scaledLow = scale_color(xMinColor, pParams);
                    ColorQuad scaledHigh = scale_color(xMaxColor, pParams);

                    float err = 0;
                    for (int i = 0; i < totalComps; i++)
                        err += squaref((scaledLow.get(i) / 255.0f) - xl.m_c[i]) + squaref((scaledHigh.get(i) / 255.0f) - xh.m_c[i]);

                    if (err < best_err) {
                        best_err = err;
                        best_pbits[0] = p;
                        best_pbits[1] = p;
                        for (int j = 0; j < 4; j++) {
                            bestMinColor.set(j, (xMinColor.get(j) >> 1));
                            bestMaxColor.set(j, (xMaxColor.get(j) >> 1));
                        }
                    }
                }
            }

            fixDegenerateEndpoints(mode, bestMinColor, bestMaxColor, xl, xh, iscalep >> 1);

            if ((pResults.m_best_overall_err == Long.MAX_VALUE) || bestMinColor.notequals(pResults.m_low_endpoint)
                    || bestMaxColor.notequals(pResults.m_high_endpoint) || (best_pbits[0] != pResults.m_pbits[0])
                    || (best_pbits[1] != pResults.m_pbits[1]))
                evaluate_solution(bestMinColor, bestMaxColor, best_pbits, pParams, pResults);
        } else {
            int iscale = (1 << pParams.m_comp_bits) - 1;
            float scale = iscale;

            ColorQuad trialMinColor = new ColorQuad(), trialMaxColor = new ColorQuad();
            trialMinColor.setClamped((int) (xl.m_c[0] * scale + .5f), (int) (xl.m_c[1] * scale + .5f), (int) (xl.m_c[2] * scale + .5f),
                    (int) (xl.m_c[3] * scale + .5f));
            trialMaxColor.setClamped((int) (xh.m_c[0] * scale + .5f), (int) (xh.m_c[1] * scale + .5f), (int) (xh.m_c[2] * scale + .5f),
                    (int) (xh.m_c[3] * scale + .5f));

            fixDegenerateEndpoints(mode, trialMinColor, trialMaxColor, xl, xh, iscale);

            if ((pResults.m_best_overall_err == Long.MAX_VALUE) || trialMinColor.notequals(pResults.m_low_endpoint)
                    || trialMaxColor.notequals(pResults.m_high_endpoint))
                evaluate_solution(trialMinColor, trialMaxColor, pResults.m_pbits, pParams, pResults);
        }

        return pResults.m_best_overall_err;
    }

    ColorCellCompressorResults color_cell_compression(int mode, ColorCellCompressorParams pParams, ColorCellCompressorResults pResults,
            BC7CompressBlockParams pComp_params) {
        assert ((mode == 6) || (!pParams.m_has_alpha));

        pResults.m_best_overall_err = Long.MAX_VALUE;

        // If the partition's colors are all the same in mode 1, then just pack them as a single color.
        if (mode == 1) {
            int cr = pParams.m_pPixels[0].get(0), cg = pParams.m_pPixels[0].get(1), cb = pParams.m_pPixels[0].get(2);

            boolean allSame = true;
            for (int i = 1; i < pParams.m_num_pixels; i++) {
                if ((cr != pParams.m_pPixels[i].get(0)) || (cg != pParams.m_pPixels[i].get(1)) || (cb != pParams.m_pPixels[i].get(2))) {
                    allSame = false;
                    break;
                }
            }

            if (allSame)
                return pack_mode1_to_one_color(pParams, pResults, cr, cg, cb, pResults.m_pSelectors);
        }

        // Compute partition's mean color and principle axis.
        Vec4f meanColor = new Vec4f(), axis = new Vec4f();
        meanColor.setScalar(0.0f);

        for (int i = 0; i < pParams.m_num_pixels; i++) {
            Vec4f color = Vec4f.fromColor(pParams.m_pPixels[i]);
            meanColor = Vec4f.add(meanColor, color);
        }

        Vec4f meanColorScaled = Vec4f.mul(meanColor, 1.0f / (pParams.m_num_pixels));

        meanColor = Vec4f.mul(meanColor, 1.0f / (pParams.m_num_pixels * 255.0f));
        meanColor.saturate_in_place();

        if (pParams.m_has_alpha) {
            // Use incremental PCA for RGBA PCA, because it's simple.
            axis.setScalar(0.0f);
            for (int i = 0; i < pParams.m_num_pixels; i++) {
                Vec4f color = Vec4f.fromColor(pParams.m_pPixels[i]);
                color = Vec4f.sub(color, meanColorScaled);
                Vec4f a = Vec4f.mul(color, color.m_c[0]);
                Vec4f b = Vec4f.mul(color, color.m_c[1]);
                Vec4f c = Vec4f.mul(color, color.m_c[2]);
                Vec4f d = Vec4f.mul(color, color.m_c[3]);
                Vec4f n = i != 0 ? axis : color;
                n.normalize_in_place();
                axis.m_c[0] += Vec4f.dot(a, n);
                axis.m_c[1] += Vec4f.dot(b, n);
                axis.m_c[2] += Vec4f.dot(c, n);
                axis.m_c[3] += Vec4f.dot(d, n);
            }
            axis.normalize_in_place();
        } else {
            // Use covar technique for RGB PCA, because it doesn't require per-pixel normalization.
            float cov[] = { 0f, 0f, 0f, 0f, 0f, 0f };

            for (int i = 0; i < pParams.m_num_pixels; i++) {
                ColorQuad pV = pParams.m_pPixels[i];
                float r = pV.get(0) - meanColorScaled.m_c[0];
                float g = pV.get(0) - meanColorScaled.m_c[1];
                float b = pV.get(0) - meanColorScaled.m_c[2];
                cov[0] += r * r;
                cov[1] += r * g;
                cov[2] += r * b;
                cov[3] += g * g;
                cov[4] += g * b;
                cov[5] += b * b;
            }

            float vfr = .9f, vfg = 1.0f, vfb = .7f;
            for (int iter = 0; iter < 3; iter++) {
                float r = vfr * cov[0] + vfg * cov[1] + vfb * cov[2];
                float g = vfr * cov[1] + vfg * cov[3] + vfb * cov[4];
                float b = vfr * cov[2] + vfg * cov[4] + vfb * cov[5];

                float m = Math.max(Math.max(Math.abs(r), Math.abs(g)), Math.abs(b));
                if (m > 1e-10f) {
                    m = 1.0f / m;
                    r *= m;
                    g *= m;
                    b *= m;
                }

                vfr = r;
                vfg = g;
                vfb = b;
            }

            float len = vfr * vfr + vfg * vfg + vfb * vfb;
            if (len < 1e-10f)
                axis.setScalar(0.0f);
            else {
                len = (float) (1.0f / Math.sqrt(len));
                vfr *= len;
                vfg *= len;
                vfb *= len;
                axis.set(vfr, vfg, vfb, 0);
            }
        }

        if (Vec4f.dot(axis, axis) < .5f) {
            if (pParams.m_perceptual)
                axis.set(.213f, .715f, .072f, pParams.m_has_alpha ? .715f : 0);
            else
                axis.set(1.0f, 1.0f, 1.0f, pParams.m_has_alpha ? 1.0f : 0);
            axis.normalize_in_place();
        }

        float l = 1e+9f, h = -1e+9f;

        for (int i = 0; i < pParams.m_num_pixels; i++) {
            Vec4f color = Vec4f.fromColor(pParams.m_pPixels[i]);

            Vec4f q = Vec4f.sub(color, meanColorScaled);
            float d = Vec4f.dot(q, axis);

            l = Math.min(l, d);
            h = Math.max(h, d);
        }

        l *= (1.0f / 255.0f);
        h *= (1.0f / 255.0f);

        Vec4f b0 = Vec4f.mul(axis, l);
        Vec4f b1 = Vec4f.mul(axis, h);
        Vec4f c0 = Vec4f.add(meanColor, b0);
        Vec4f c1 = Vec4f.add(meanColor, b1);
        Vec4f minColor = Vec4f.saturate(c0);
        Vec4f maxColor = Vec4f.saturate(c1);

        Vec4f whiteVec = new Vec4f();
        whiteVec.setScalar(1.0f);
        if (Vec4f.dot(minColor, whiteVec) > Vec4f.dot(maxColor, whiteVec)) {
            Vec4f temp = minColor;
            minColor = maxColor;
            maxColor = temp;
        }
        // First find a solution using the block's PCA.
        if (find_optimal_solution(mode, minColor, maxColor, pParams, pResults) == 0)
            return pResults;

        if (pComp_params.tryLeastSquares) {
            // Now try to refine the solution using least squares by computing the optimal endpoints from the current selectors.
            Vec4f xl = new Vec4f(), xh = new Vec4f();
            xl.setScalar(0.0f);
            xh.setScalar(0.0f);
            if (pParams.m_has_alpha)
                compute_least_squares_endpoints_rgba(pParams.m_num_pixels, pResults.m_pSelectors, pParams.m_pSelector_weightsx, xl, xh,
                        pParams.m_pPixels);
            else
                compute_least_squares_endpoints_rgb(pParams.m_num_pixels, pResults.m_pSelectors, pParams.m_pSelector_weightsx, xl, xh,
                        pParams.m_pPixels);

            xl = Vec4f.mul(xl, (1.0f / 255.0f));
            xh = Vec4f.mul(xh, (1.0f / 255.0f));

            if (find_optimal_solution(mode, xl, xh, pParams, pResults) == 0)
                return pResults;
        }

        if (pComp_params.uberLevel > 0) {
            // In uber level 1, try varying the selectors a little, somewhat like cluster fit would. First try incrementing the minimum
            // selectors,
            // then try decrementing the selectrors, then try both.
            byte selectors_temp[] = new byte[16], selectors_temp1[] = new byte[16];
            System.arraycopy(pResults.m_pSelectors, 0, selectors_temp, 0, pParams.m_num_pixels);

            int max_selector = pParams.m_num_selector_weights - 1;

            int min_sel = 16;
            int max_sel = 0;
            for (int i = 0; i < pParams.m_num_pixels; i++) {
                int sel = selectors_temp[i];
                min_sel = Math.min(min_sel, sel);
                max_sel = Math.max(max_sel, sel);
            }

            for (int i = 0; i < pParams.m_num_pixels; i++) {
                int sel = selectors_temp[i];
                if ((sel == min_sel) && (sel < (pParams.m_num_selector_weights - 1)))
                    sel++;
                selectors_temp1[i] = (byte) sel;
            }

            Vec4f xl = new Vec4f(), xh = new Vec4f();
            xl.setScalar(0.0f);
            xh.setScalar(0.0f);
            if (pParams.m_has_alpha)
                compute_least_squares_endpoints_rgba(pParams.m_num_pixels, selectors_temp1, pParams.m_pSelector_weightsx, xl, xh,
                        pParams.m_pPixels);
            else
                compute_least_squares_endpoints_rgb(pParams.m_num_pixels, selectors_temp1, pParams.m_pSelector_weightsx, xl, xh,
                        pParams.m_pPixels);

            xl = Vec4f.mul(xl, (1.0f / 255.0f));
            xh = Vec4f.mul(xh, (1.0f / 255.0f));

            if (find_optimal_solution(mode, xl, xh, pParams, pResults) == 0)
                return pResults;

            for (int i = 0; i < pParams.m_num_pixels; i++) {
                int sel = selectors_temp[i];
                if ((sel == max_sel) && (sel > 0))
                    sel--;
                selectors_temp1[i] = (byte) sel;
            }

            if (pParams.m_has_alpha)
                compute_least_squares_endpoints_rgba(pParams.m_num_pixels, selectors_temp1, pParams.m_pSelector_weightsx, xl, xh,
                        pParams.m_pPixels);
            else
                compute_least_squares_endpoints_rgb(pParams.m_num_pixels, selectors_temp1, pParams.m_pSelector_weightsx, xl, xh,
                        pParams.m_pPixels);

            xl = Vec4f.mul(xl, (1.0f / 255.0f));
            xh = Vec4f.mul(xh, (1.0f / 255.0f));

            if (find_optimal_solution(mode, xl, xh, pParams, pResults) == 0)
                return pResults;

            for (int i = 0; i < pParams.m_num_pixels; i++) {
                int sel = selectors_temp[i];
                if ((sel == min_sel) && (sel < (pParams.m_num_selector_weights - 1)))
                    sel++;
                else if ((sel == max_sel) && (sel > 0))
                    sel--;
                selectors_temp1[i] = (byte) sel;
            }

            if (pParams.m_has_alpha)
                compute_least_squares_endpoints_rgba(pParams.m_num_pixels, selectors_temp1, pParams.m_pSelector_weightsx, xl, xh,
                        pParams.m_pPixels);
            else
                compute_least_squares_endpoints_rgb(pParams.m_num_pixels, selectors_temp1, pParams.m_pSelector_weightsx, xl, xh,
                        pParams.m_pPixels);

            xl = Vec4f.mul(xl, (1.0f / 255.0f));
            xh = Vec4f.mul(xh, (1.0f / 255.0f));

            if (find_optimal_solution(mode, xl, xh, pParams, pResults) == 0)
                return pResults;

            // In uber levels 2+, try taking more advantage of endpoint extrapolation by scaling the selectors in one direction or another.
            int uber_err_thresh = (pParams.m_num_pixels * 56) >> 4;
            if ((pComp_params.uberLevel >= 2) && (pResults.m_best_overall_err > uber_err_thresh)) {
                int Q = (pComp_params.uberLevel >= 4) ? (pComp_params.uberLevel - 2) : 1;
                for (int ly = -Q; ly <= 1; ly++) {
                    for (int hy = max_selector - 1; hy <= (max_selector + Q); hy++) {
                        if ((ly == 0) && (hy == max_selector))
                            continue;

                        for (int i = 0; i < pParams.m_num_pixels; i++)
                            selectors_temp1[i] = (byte) clampf(
                                    (float) Math.floor(max_selector * ((float) selectors_temp[i] - ly) / ((float) hy - (float) ly) + .5f),
                                    0, max_selector);

                        // Vec4f xl, xh;
                        xl.setScalar(0.0f);
                        xh.setScalar(0.0f);
                        if (pParams.m_has_alpha)
                            compute_least_squares_endpoints_rgba(pParams.m_num_pixels, selectors_temp1, pParams.m_pSelector_weightsx, xl,
                                    xh, pParams.m_pPixels);
                        else
                            compute_least_squares_endpoints_rgb(pParams.m_num_pixels, selectors_temp1, pParams.m_pSelector_weightsx, xl, xh,
                                    pParams.m_pPixels);

                        xl = Vec4f.mul(xl, (1.0f / 255.0f));
                        xh = Vec4f.mul(xh, (1.0f / 255.0f));

                        if (find_optimal_solution(mode, xl, xh, pParams, pResults) == 0)
                            return pResults;
                    }
                }
            }
        }

        if (mode == 1) {
            // Try encoding the partition as a single color by using the optimal singe colors tables to encode the block to its mean.
            ColorCellCompressorResults avg_results = new ColorCellCompressorResults(pResults);
            int r = (int) (.5f + meanColor.m_c[0] * 255.0f), g = (int) (.5f + meanColor.m_c[1] * 255.0f),
                    b = (int) (.5f + meanColor.m_c[2] * 255.0f);
            avg_results = pack_mode1_to_one_color(pParams, avg_results, r, g, b, pResults.m_pSelectors_temp);
            if (avg_results.m_best_overall_err < pResults.m_best_overall_err) {
                pResults = avg_results;
                System.arraycopy(pResults.m_pSelectors_temp, 0, pResults.m_pSelectors, 0, pParams.m_num_pixels);
                // pResults.m_best_overall_err = avg_err;
            }
        }

        return pResults;
    }

    long color_cell_compression_est(int num_pixels, ColorQuad[] pPixels, boolean perceptual, int[] pweights, long best_err_so_far) {
        // Find RGB bounds as an approximation of the block's principle axis
        int lr = 255, lg = 255, lb = 255;
        int hr = 0, hg = 0, hb = 0;
        for (int i = 0; i < num_pixels; i++) {
            ColorQuad pC = pPixels[i];
            if (pC.get(0) < lr)
                lr = pC.get(0);
            if (pC.get(1) < lg)
                lg = pC.get(1);
            if (pC.get(2) < lb)
                lb = pC.get(2);
            if (pC.get(0) > hr)
                hr = pC.get(0);
            if (pC.get(1) > hg)
                hg = pC.get(1);
            if (pC.get(2) > hb)
                hb = pC.get(2);
        }

        ColorQuad lowColor = new ColorQuad();
        lowColor.set(lr, lg, lb, 0);
        ColorQuad highColor = new ColorQuad();
        highColor.set(hr, hg, hb, 0);

        // Place endpoints at bbox diagonals and compute interpolated colors
        int N = 8;
        ColorQuad weightedColors[] = new ColorQuad[8];

        weightedColors[0] = lowColor;
        weightedColors[N - 1] = highColor;
        for (int i = 1; i < (N - 1); i++) {
            weightedColors[i] = new ColorQuad();
            weightedColors[i].set(0, ((lowColor.get(0) * (64 - g_bc7_weights3[i]) + highColor.get(0) * g_bc7_weights3[i] + 32) >> 6));
            weightedColors[i].set(1, ((lowColor.get(1) * (64 - g_bc7_weights3[i]) + highColor.get(1) * g_bc7_weights3[i] + 32) >> 6));
            weightedColors[i].set(2, ((lowColor.get(2) * (64 - g_bc7_weights3[i]) + highColor.get(2) * g_bc7_weights3[i] + 32) >> 6));
        }

        // Compute dots and thresholds
        int ar = highColor.get(0) - lowColor.get(0);
        int ag = highColor.get(1) - lowColor.get(1);
        int ab = highColor.get(2) - lowColor.get(2);

        int dots[] = new int[8];
        for (int i = 0; i < N; i++)
            dots[i] = weightedColors[i].get(0) * ar + weightedColors[i].get(1) * ag + weightedColors[i].get(2) * ab;

        int thresh[] = new int[8 - 1];
        for (int i = 0; i < (N - 1); i++)
            thresh[i] = (dots[i] + dots[i + 1] + 1) >> 1;

        long total_err = 0;
        if (perceptual) {
            // Transform block's interpolated colors to YCbCr
            int[] l1 = new int[8], cr1 = new int[8], cb1 = new int[8];
            for (int j = 0; j < 8; j++) {
                ColorQuad pE1 = weightedColors[j];
                l1[j] = pE1.get(0) * 109 + pE1.get(1) * 366 + pE1.get(2) * 37;
                cr1[j] = (pE1.get(0) << 9) - l1[j];
                cb1[j] = (pE1.get(2) << 9) - l1[j];
            }

            for (int i = 0; i < num_pixels; i++) {
                ColorQuad pC = pPixels[i];

                int d = ar * pC.get(0) + ag * pC.get(1) + ab * pC.get(2);

                // Find approximate selector
                int s = 0;
                if (d >= thresh[6])
                    s = 7;
                else if (d >= thresh[5])
                    s = 6;
                else if (d >= thresh[4])
                    s = 5;
                else if (d >= thresh[3])
                    s = 4;
                else if (d >= thresh[2])
                    s = 3;
                else if (d >= thresh[1])
                    s = 2;
                else if (d >= thresh[0])
                    s = 1;

                // Compute error
                int l2 = pC.get(0) * 109 + pC.get(1) * 366 + pC.get(2) * 37;
                int cr2 = (pC.get(0) << 9) - l2;
                int cb2 = (pC.get(2) << 9) - l2;

                int dl = (l1[s] - l2) >> 8;
                int dcr = (cr1[s] - cr2) >> 8;
                int dcb = (cb1[s] - cb2) >> 8;

                int ie = (pweights[0] * dl * dl) + (pweights[1] * dcr * dcr) + (pweights[2] * dcb * dcb);

                total_err += ie;
                if (total_err > best_err_so_far)
                    break;
            }
        } else {
            for (int i = 0; i < num_pixels; i++) {
                ColorQuad pC = pPixels[i];

                int d = ar * pC.get(0) + ag * pC.get(1) + ab * pC.get(2);

                // Find approximate selector
                int s = 0;
                if (d >= thresh[6])
                    s = 7;
                else if (d >= thresh[5])
                    s = 6;
                else if (d >= thresh[4])
                    s = 5;
                else if (d >= thresh[3])
                    s = 4;
                else if (d >= thresh[2])
                    s = 3;
                else if (d >= thresh[1])
                    s = 2;
                else if (d >= thresh[0])
                    s = 1;

                // Compute error
                ColorQuad pE1 = weightedColors[s];

                int dr = pE1.get(0) - pC.get(0);
                int dg = pE1.get(1) - pC.get(1);
                int db = pE1.get(2) - pC.get(2);

                total_err += pweights[0] * (dr * dr) + pweights[1] * (dg * dg) + pweights[2] * (db * db);
                if (total_err > best_err_so_far)
                    break;
            }
        }

        return total_err;
    }

    // Estimate the partition used by mode 1. This scans through each partition and computes an approximate error for each.
    int estimate_partition(ColorQuad[] pPixels, BC7CompressBlockParams pComp_params, int pweights[]) {
        int total_partitions = Math.min(pComp_params.maxPartitionsMode1, MAX_PARTITIONS1);
        if (total_partitions <= 1)
            return 0;

        long best_err = Long.MAX_VALUE;
        int best_partition = 0;
        int best_key_partition = 0;

        for (int partition_iter = 0; (partition_iter < total_partitions) && (best_err > 0); partition_iter++) {
            int partition = s_sorted_partition_order[partition_iter];

            // Check to see if we should bother evaluating this partition at all, depending on the best partition found
            // from the first 14.
            if (pComp_params.m_mode1_partition_estimation_filterbank) {
                if ((partition_iter >= 14) && (partition_iter <= 34)) {
                    int best_key_partition_bitmask = 1 << (best_key_partition + 1);
                    if ((g_partition_predictors[partition] & best_key_partition_bitmask) == 0) {
                        if (partition_iter == 34)
                            break;

                        continue;
                    }
                }
            }

            int partitionBase = partition * 16;

            ColorQuad[][] subset_colors = new ColorQuad[2][16];
            int subset_total_colors[] = { 0, 0 };
            for (int index = 0; index < 16; index++)
                subset_colors[g_bc7_partition2[partitionBase
                        + index]][subset_total_colors[g_bc7_partition2[partitionBase + index]]++] = pPixels[index];

            long total_subset_err = 0;
            for (int subset = 0; (subset < 2) && (total_subset_err < best_err); subset++)
                total_subset_err += color_cell_compression_est(subset_total_colors[subset], subset_colors[subset], pComp_params.perceptual,
                        pweights, best_err);

            if (total_subset_err < best_err) {
                best_err = total_subset_err;
                best_partition = partition;
            }
            // If the checkerboard pattern doesn't get the highest ranking vs. the previous (lower frequency) patterns,
            // then just stop now because statistically the subsequent patterns won't do well either.
            if ((partition == 34) && (best_partition != 34)) {
                break;
            }
            if (partition_iter == 13)
                best_key_partition = best_partition;

        } // partition

        return best_partition;
    }

    int set_block_bits(byte[] pBytes, int val, int num_bits, int pCur_ofs) {
        assert ((num_bits <= 32) && (val < (1 << num_bits)));
        while (num_bits > 0) {
            int n = Math.min(8 - (pCur_ofs & 7), num_bits);
            pBytes[pCur_ofs >> 3] |= (val << (pCur_ofs & 7));
            val >>= n;
            num_bits -= n;
            pCur_ofs += n;
        }
        assert (pCur_ofs <= 128);
        return pCur_ofs;
    }

    private class OptimizationResults {
        int m_mode;
        int m_partition;
        final byte[] m_selectors = new byte[16];
        final ColorQuad[] m_low = new ColorQuad[] { new ColorQuad(), new ColorQuad() };
        final ColorQuad[] m_high = new ColorQuad[] { new ColorQuad(), new ColorQuad() };
        final int[][] m_pbits = new int[2][2];
    }

    void encode_bc7_block(byte[] pBlock, OptimizationResults pResults) {
        int best_mode = pResults.m_mode;
        int total_subsets = g_bc7_num_subsets[best_mode];
        int total_partitions = 1 << g_bc7_partition_bits[best_mode];
        byte[] pPartition = (total_subsets == 2) ? g_bc7_partition2 : g_bc7_partition1;
        int pPartitionOffset = (total_subsets == 2) ? pResults.m_partition * 16 : 0;

        byte color_selectors[] = Arrays.copyOf(pResults.m_selectors, 16);

        ColorQuad[] low = Arrays.copyOf(pResults.m_low, pResults.m_low.length);
        ColorQuad[] high = Arrays.copyOf(pResults.m_high, pResults.m_high.length);

        int[][] pbits = new int[2][2];
        pbits[0] = Arrays.copyOf(pResults.m_pbits[0], pResults.m_pbits[0].length);
        pbits[1] = Arrays.copyOf(pResults.m_pbits[1], pResults.m_pbits[1].length);

        int anchor[] = { -1, -1 };

        for (int k = 0; k < total_subsets; k++) {
            int anchor_index = k != 0 ? g_bc7_table_anchor_index_second_subset[pResults.m_partition] : 0;
            anchor[k] = anchor_index;

            int color_index_bits = get_bc7_color_index_size(best_mode, 0);
            int num_color_indices = 1 << color_index_bits;

            if ((color_selectors[anchor_index] & (num_color_indices >> 1)) != 0) {
                for (int i = 0; i < 16; i++)
                    if (pPartition[pPartitionOffset + i] == k)
                        color_selectors[i] = (byte) ((num_color_indices - 1) - color_selectors[i]);

                ColorQuad tmp = low[k];
                low[k] = high[k];
                high[k] = tmp;

                if (g_bc7_mode_has_shared_p_bits[best_mode] == 0) {
                    int t = pbits[k][0];
                    pbits[k][0] = pbits[k][1];
                    pbits[k][1] = t;
                }
            }
        }

        Arrays.fill(pBlock, (byte) 0);

        int cur_bit_ofs = 0;
        cur_bit_ofs = set_block_bits(pBlock, 1 << best_mode, best_mode + 1, cur_bit_ofs);

        if (total_partitions > 1)
            cur_bit_ofs = set_block_bits(pBlock, pResults.m_partition, 6, cur_bit_ofs);

        int total_comps = (best_mode >= 4) ? 4 : 3;
        for (int comp = 0; comp < total_comps; comp++) {
            for (int subset = 0; subset < total_subsets; subset++) {
                cur_bit_ofs = set_block_bits(pBlock, low[subset].get(comp),
                        (comp == 3) ? g_bc7_alpha_precision_table[best_mode] : g_bc7_color_precision_table[best_mode], cur_bit_ofs);
                cur_bit_ofs = set_block_bits(pBlock, high[subset].get(comp),
                        (comp == 3) ? g_bc7_alpha_precision_table[best_mode] : g_bc7_color_precision_table[best_mode], cur_bit_ofs);
            }
        }

        for (int subset = 0; subset < total_subsets; subset++) {
            cur_bit_ofs = set_block_bits(pBlock, pbits[subset][0], 1, cur_bit_ofs);
            if (g_bc7_mode_has_shared_p_bits[best_mode] == 0)
                cur_bit_ofs = set_block_bits(pBlock, pbits[subset][1], 1, cur_bit_ofs);
        }

        for (int idx = 0; idx < 16; idx++) {
            int n = get_bc7_color_index_size(best_mode, 0);
            if ((idx == anchor[0]) || (idx == anchor[1]))
                n--;
            cur_bit_ofs = set_block_bits(pBlock, color_selectors[idx], n, cur_bit_ofs);
        }

        assert (cur_bit_ofs == 128);
    }

    void handle_alpha_block(byte[] pBlock, ColorQuad[] pPixels, BC7CompressBlockParams pComp_params, ColorCellCompressorParams pParams) {
        ColorCellCompressorResults results6 = new ColorCellCompressorResults();

        pParams.m_pSelector_weights = g_bc7_weights4;
        pParams.m_pSelector_weightsx = g_bc7_weights4x;
        pParams.m_num_selector_weights = 16;
        pParams.m_comp_bits = 7;
        pParams.m_has_pbits = true;
        pParams.m_has_alpha = true;
        pParams.m_perceptual = pComp_params.perceptual;
        pParams.m_num_pixels = 16;
        pParams.m_pPixels = pPixels;

        OptimizationResults opt_results = new OptimizationResults();
        results6.m_pSelectors = opt_results.m_selectors;

        byte selectors_temp[] = new byte[16];
        results6.m_pSelectors_temp = selectors_temp;

        color_cell_compression(6, pParams, results6, pComp_params);

        opt_results.m_mode = 6;
        opt_results.m_partition = 0;
        opt_results.m_low[0] = results6.m_low_endpoint;
        opt_results.m_high[0] = results6.m_high_endpoint;
        opt_results.m_pbits[0][0] = results6.m_pbits[0];
        opt_results.m_pbits[0][1] = results6.m_pbits[1];

        encode_bc7_block(pBlock, opt_results);
    }

    void handle_opaque_block(byte[] pBlock, ColorQuad[] pPixels, BC7CompressBlockParams pComp_params, ColorCellCompressorParams pParams) {
        byte[] selectors_temp = new byte[16];

        // Mode 6
        OptimizationResults opt_results = new OptimizationResults();

        pParams.m_pSelector_weights = g_bc7_weights4;
        pParams.m_pSelector_weightsx = g_bc7_weights4x;
        pParams.m_num_selector_weights = 16;
        pParams.m_comp_bits = 7;
        pParams.m_has_pbits = true;
        pParams.m_endpoints_share_pbit = false;
        pParams.m_perceptual = pComp_params.perceptual;
        pParams.m_num_pixels = 16;
        pParams.m_pPixels = pPixels;
        pParams.m_has_alpha = false;

        ColorCellCompressorResults results6 = new ColorCellCompressorResults();
        results6.m_pSelectors = opt_results.m_selectors;
        results6.m_pSelectors_temp = selectors_temp;

        long best_err = color_cell_compression(6, pParams, results6, pComp_params).m_best_overall_err;

        opt_results.m_mode = 6;
        opt_results.m_partition = 0;
        opt_results.m_low[0] = results6.m_low_endpoint;
        opt_results.m_high[0] = results6.m_high_endpoint;
        opt_results.m_pbits[0][0] = results6.m_pbits[0];
        opt_results.m_pbits[0][1] = results6.m_pbits[1];

        // Mode 1
        if ((best_err > 0) && (pComp_params.maxPartitionsMode1 > 0)) {
            int trial_partition = estimate_partition(pPixels, pComp_params, pParams.m_weights);

            pParams.m_pSelector_weights = g_bc7_weights3;
            pParams.m_pSelector_weightsx = g_bc7_weights3x;
            pParams.m_num_selector_weights = 8;
            pParams.m_comp_bits = 6;
            pParams.m_has_pbits = true;
            pParams.m_endpoints_share_pbit = true;

            int pPartition = trial_partition * 16;

            ColorQuad[][] subset_colors = new ColorQuad[2][16];
            for (int i1 = 0; i1 < 2; i1++) {
                for (int i2 = 0; i2 < 16; i2++) {
                    subset_colors[i1][i2] = new ColorQuad();
                }
            }

            int subset_total_colors1[] = { 0, 0 };

            byte subset_pixel_index1[][] = new byte[2][16];
            byte subset_selectors1[][] = new byte[2][16];
            ColorCellCompressorResults[] subset_results1 = new ColorCellCompressorResults[] { new ColorCellCompressorResults(),
                    new ColorCellCompressorResults() };

            for (int idx = 0; idx < 16; idx++) {
                int p = g_bc7_partition2[pPartition + idx];
                subset_colors[p][subset_total_colors1[p]] = pPixels[idx];
                subset_pixel_index1[p][subset_total_colors1[p]] = (byte) idx;
                subset_total_colors1[p]++;
            }

            long trial_err = 0;
            for (int subset = 0; subset < 2; subset++) {
                pParams.m_num_pixels = subset_total_colors1[subset];
                pParams.m_pPixels = subset_colors[subset];

                ColorCellCompressorResults pResults = subset_results1[subset];
                pResults.m_pSelectors = subset_selectors1[subset];
                pResults.m_pSelectors_temp = selectors_temp;
                long err = color_cell_compression(1, pParams, pResults, pComp_params).m_best_overall_err;
                trial_err += err;
                if (trial_err > best_err)
                    break;

            } // subset

            if (trial_err < best_err) {
                best_err = trial_err;
                opt_results.m_mode = 1;
                opt_results.m_partition = trial_partition;
                for (int subset = 0; subset < 2; subset++) {
                    for (int i = 0; i < subset_total_colors1[subset]; i++)
                        opt_results.m_selectors[subset_pixel_index1[subset][i]] = subset_selectors1[subset][i];
                    opt_results.m_low[subset] = subset_results1[subset].m_low_endpoint;
                    opt_results.m_high[subset] = subset_results1[subset].m_high_endpoint;
                    opt_results.m_pbits[subset][0] = subset_results1[subset].m_pbits[0];
                }
            }
        }

        encode_bc7_block(pBlock, opt_results);
    }

    /**
     *
     * @param dst
     *            array to store the compressed data in, at least 16 bytes long
     * @param src
     *            input data, 4x4 block of RGBA (4 bytes) data, should be 64 bytes in total
     * @param compParams
     *            compression parameters
     * @return true if alpha compressions used, false otherwise
     */
    public boolean compressBlock(byte[] dst, byte[] src, BC7CompressBlockParams compParams) {
        if (dst.length < 16) {
            throw new IllegalArgumentException("Output buffer too small");
        }
        if (src.length < 64) {
            throw new IllegalArgumentException("Input buffer too small");
        }
        ColorQuad[] pPixels = new ColorQuad[src.length / 4];
        for (int i = 0; i < pPixels.length; i++) {
            pPixels[i] = new ColorQuad(src[i * 4], src[i * 4 + 1], src[i * 4 + 2], src[i * 4 + 3]);
        }

        ColorCellCompressorParams params = new ColorCellCompressorParams();
        if (compParams.perceptual) {
            // https://en.wikipedia.org/wiki/YCbCr#ITU-R_BT.709_conversion
            float pr_weight = (.5f / (1.0f - .2126f)) * (.5f / (1.0f - .2126f));
            float pb_weight = (.5f / (1.0f - .0722f)) * (.5f / (1.0f - .0722f));
            params.m_weights[0] = (int) (compParams.weights[0] * 4.0f);
            params.m_weights[1] = (int) (compParams.weights[1] * 4.0f * pr_weight);
            params.m_weights[2] = (int) (compParams.weights[2] * 4.0f * pb_weight);
            params.m_weights[3] = compParams.weights[3] * 4;
        } else
            System.arraycopy(compParams.weights, 0, params.m_weights, 0, params.m_weights.length);

        if (!compParams.opaque) {
            for (int i = 0; i < 16; i++) {
                if (pPixels[i].get(3) < 255) {
                    handle_alpha_block(dst, pPixels, compParams, params);
                    return true;
                }
            }
        }
        handle_opaque_block(dst, pPixels, compParams, params);
        return false;
    }

}
