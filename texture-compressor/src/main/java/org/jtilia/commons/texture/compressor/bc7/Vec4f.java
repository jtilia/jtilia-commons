/*
 * This code is based on https://github.com/richgel999/bc7enc16
 * Copyright(c) 2018 Richard Geldreich, Jr.
 * Java port copyright (c) 2018 Piotr Piastucki piotr@jtilia.org
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files(the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and / or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions :
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.jtilia.commons.texture.compressor.bc7;

class Vec4f {
    final float m_c[];

    Vec4f() {
        m_c = new float[4];
    }

    Vec4f(float x, float y, float z, float w) {
        m_c = new float[] { x, y, z, w };
    }

    static Vec4f add(Vec4f a, Vec4f b) {
        return new Vec4f(a.m_c[0] + b.m_c[0], a.m_c[1] + b.m_c[1], a.m_c[2] + b.m_c[2], a.m_c[3] + b.m_c[3]);
    }

    static Vec4f sub(Vec4f a, Vec4f b) {
        return new Vec4f(a.m_c[0] - b.m_c[0], a.m_c[1] - b.m_c[1], a.m_c[2] - b.m_c[2], a.m_c[3] - b.m_c[3]);
    }

    static float dot(Vec4f a, Vec4f b) {
        return a.m_c[0] * b.m_c[0] + a.m_c[1] * b.m_c[1] + a.m_c[2] * b.m_c[2] + a.m_c[3] * b.m_c[3];
    }

    static Vec4f saturate(Vec4f v) {
        return new Vec4f(Utils.saturate(v.m_c[0]), Utils.saturate(v.m_c[1]), Utils.saturate(v.m_c[2]), Utils.saturate(v.m_c[3]));
    }

    Vec4f set(float x, float y, float z, float w) {
        m_c[0] = x;
        m_c[1] = y;
        m_c[2] = z;
        m_c[3] = w;
        return this;
    }

    Vec4f setScalar(float x) {
        m_c[0] = x;
        m_c[1] = x;
        m_c[2] = x;
        m_c[3] = x;
        return this;
    }

    Vec4f saturate_in_place() {
        m_c[0] = Utils.saturate(m_c[0]);
        m_c[1] = Utils.saturate(m_c[1]);
        m_c[2] = Utils.saturate(m_c[2]);
        m_c[3] = Utils.saturate(m_c[3]);
        return this;
    }

    static Vec4f fromColor(ColorQuad pC) {
        return new Vec4f(pC.get(0), pC.get(1), pC.get(2), pC.get(3));
    }

    static Vec4f mul(Vec4f v, float s) {
        return new Vec4f(v.m_c[0] * s, v.m_c[1] * s, v.m_c[2] * s, v.m_c[3] * s);
    }

    Vec4f normalize_in_place() {
        float s = m_c[0] * m_c[0] + m_c[1] * m_c[1] + m_c[2] * m_c[2] + m_c[3] * m_c[3];
        if (s != 0.0f) {
            s = (float) (1.0f / Math.sqrt(s));
            m_c[0] *= s;
            m_c[1] *= s;
            m_c[2] *= s;
            m_c[3] *= s;
        }
        return this;
    }

}