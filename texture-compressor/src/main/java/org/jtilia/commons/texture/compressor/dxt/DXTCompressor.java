/*
 * This code is based on https://github.com/nothings/stb
 * Copyright (c) 2017 Sean Barrett
 * Java port copyright (c) 2018 Piotr Piastucki piotr@jtilia.org
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.jtilia.commons.texture.compressor.dxt;

public abstract class DXTCompressor {

    // private final boolean STB_DXT_USE_ROUNDING_BIAS = false;
    // STB_DXT_USE_ROUNDING_BIAS
    // use a rounding bias during color interpolation. this is closer to what "ideal"
    // interpolation would do but doesn't match the S3TC/DX10 spec. old versions (pre-1.03)
    // implicitly had this turned on.
    //
    // in case you're targeting a specific type of hardware (e.g. console programmers):
    // NVidia and Intel GPUs (as of 2010) as well as DX9 ref use DXT decoders that are closer
    // to STB_DXT_USE_ROUNDING_BIAS. AMD/ATI, S3 and DX10 ref are closer to rounding with no bias.
    // you also see "(a*5 + b*3) / 8" on some old GPU designs.
    // #define STB_DXT_USE_ROUNDING_BIAS

    private final int expand5[] = new int[32];
    private final int expand6[] = new int[64];

    private final int oMatch5[][];
    private final int oMatch6[][];
    // ^some magic to save a lot of multiplies in the accumulating loop...
    // (precomputed products of weights for least squares system, accumulated
    // inside one 32-bit register)
    private final int[] w1Tab = { 3, 0, 2, 1 };
    private final int[] prods = { 0x090000, 0x000900, 0x040102, 0x010402 };
    private final byte[] const_alpha_block = { 0x49, (byte) 0x92, 0x24, 0x49, (byte) 0x92, 0x24 };

    protected DXTCompressor() {
        int i;
        for (i = 0; i < 32; i++) {
            expand5[i] = (i << 3) | (i >> 2);
        }
        for (i = 0; i < 64; i++) {
            expand6[i] = (i << 2) | (i >> 4);
        }
        oMatch5 = prepareOptTable(expand5, 32);
        oMatch6 = prepareOptTable(expand6, 64);
    }

    private int mul8Bit(int a, int b) {
        int t = a * b + 128;
        return (t + (t >> 8)) >> 8;
    }

    // compute table to reproduce constant colors as accurately as possible
    private int[][] prepareOptTable(int[] expand, int size) {
        int table[][] = new int[256][2];
        for (int i = 0; i < 256; i++) {
            int bestErr = 256;
            for (int mn = 0; mn < size; mn++) {
                for (int mx = 0; mx < size; mx++) {
                    int mine = expand[mn];
                    int maxe = expand[mx];
                    int err = Math.abs(lerp13(maxe, mine) - i);
                    // DX10 spec says that interpolation must be within 3% of
                    // "correct" result,
                    // add this as error term. (normally we'd expect a random
                    // distribution of
                    // +-1.5% error, but nowhere in the spec does it say that
                    // the error has to be
                    // unbiased - better safe than sorry).
                    err += Math.abs(maxe - mine) * 3 / 100;
                    if (err < bestErr) {
                        table[i][0] = mx;
                        table[i][1] = mn;
                        bestErr = err;
                    }
                }
            }
        }
        return table;
    }

    // linear interpolation at 1/3 point between a and b, using desired rounding type
    private int lerp13(int a, int b) {
        // if (STB_DXT_USE_ROUNDING_BIAS) {
        // with rounding bias
        // return 0;// a + stb__Mul8Bit(b-a, 0x55);
        // } else {
        // without rounding bias
        // replace "/ 3" by "* 0xaaab) >> 17" if your compiler sucks or you
        // really need every ounce of speed.
        return (2 * a + b) / 3;
        // }
    }

    protected void compressDxtBlock(byte[] dest, byte[] src, boolean alpha, boolean highQuality) {
        int offset = 0;
        if (alpha) {
            compressAlphaBlock(dest, 0, src, 3, 4);
            offset += 8;
        }
        compressColorBlock(dest, offset, src, highQuality);
    }

    // Alpha block compression (this is easy for a change)
    protected void compressAlphaBlock(byte[] dest, int destOffset, byte[] block, int blockOffset, int stride) {
        // find min/max color
        int mn, mx;
        mn = mx = block[blockOffset] & 0xff;
        for (int i = 1; i < 16; i++) {
            int b = block[blockOffset + i * stride] & 0xff;
            if (b < mn) {
                mn = b;
            } else if (b > mx) {
                mx = b;
            }
        }

        // encode them
        dest[destOffset++] = (byte) mx;
        dest[destOffset++] = (byte) mn;
        if (mx == mn) {
            System.arraycopy(const_alpha_block, 0, dest, destOffset, 6);
            return;
        }

        // determine bias and emit color indices
        // given the choice of mx/mn, these indices are optimal:
        // http://fgiesen.wordpress.com/2009/12/15/dxt5-alpha-block-index-determination/
        int dist = mx - mn;
        int dist4 = dist * 4;
        int dist2 = dist * 2;
        int bias = (dist < 8) ? (dist - 1) : (dist / 2 + 2);
        bias -= mn * 7;
        int bits = 0, mask = 0;
        for (int i = 0; i < 16; i++) {
            int a = (block[blockOffset + i * stride] & 0xff) * 7 + bias;
            int ind = a;
            // select index. this is a "linear scale" lerp factor between 0
            // (val=min) and 7 (val=max).
            int t = (a >= dist4) ? -1 : 0;
            ind = t & 4;
            a -= dist4 & t;
            t = (a >= dist2) ? -1 : 0;
            ind += t & 2;
            a -= dist2 & t;
            ind += (a >= dist) ? 1 : 0;
            // turn linear scale into DXT index (0/1 are extremal pts)
            ind = -ind & 7;
            ind ^= (2 > ind) ? 1 : 0;

            // write index
            mask |= ind << bits;
            if ((bits += 3) >= 8) {
                dest[destOffset++] = (byte) mask;
                mask >>= 8;
                bits -= 8;
            }
        }
    }

    protected void compressColorBlock(byte[] dest, int destOffset, byte[] src, boolean highQuality) {
        int[] colorBlock = new int[16];
        for (int i = 0; i < 16; i++) {
            colorBlock[i] = (src[i * 4] & 0xff) | ((src[i * 4 + 1] & 0xff) << 8) | ((src[i * 4 + 2] & 0xff) << 16);
        }
        compressColorBlock(dest, destOffset, colorBlock, highQuality);
    }

    // Color block compression
    protected void compressColorBlock(byte[] dest, int destOffset, int[] block, boolean highQuality) {
        int mask;
        int max16, min16;
        // check if block is constant
        int i;
        int block0 = block[0];
        for (i = 1; i < 16; i++) {
            if (block[i] != block0) {
                break;
            }
        }
        if (i == 16) { // constant color
            int r = block0 & 0xff, g = (block0 >> 8) & 0xff, b = (block0 >> 16) & 0xff;
            mask = 0xaaaaaaaa;
            max16 = (oMatch5[r][0] << 11) | (oMatch6[g][0] << 5) | oMatch5[b][0];
            min16 = (oMatch5[r][1] << 11) | (oMatch6[g][1] << 5) | oMatch5[b][1];
        } else {
            // pca+map along principal axis
            int minmax = optimizeColorsBlock(block);
            min16 = minmax & 0xFFFF;
            max16 = (minmax >> 16) & 0xFFFF;

            int[] color = new int[16];
            if (max16 != min16) {
                evalColors(color, max16, min16);
                mask = matchColorsBlock(block, color);
            } else {
                mask = 0;
            }

            // third step: refine (multiple times if requested)
            int refinecount = highQuality ? 2 : 1;
            for (i = 0; i < refinecount; i++) {
                int lastmask = mask;
                int minmax2 = refineBlock(block, mask, max16, min16);
                if (minmax2 != minmax) {
                    min16 = minmax2 & 0xFFFF;
                    max16 = (minmax2 >> 16) & 0xFFFF;
                    if (max16 != min16) {
                        evalColors(color, max16, min16);
                        mask = matchColorsBlock(block, color);
                    } else {
                        mask = 0;
                        break;
                    }
                }
                if (mask == lastmask) {
                    break;
                }
            }
        }

        // write the color block
        if (max16 < min16) {
            int t = min16;
            min16 = max16;
            max16 = t;
            mask ^= 0x55555555;
        }

        dest[destOffset + 0] = (byte) (max16);
        dest[destOffset + 1] = (byte) (max16 >> 8);
        dest[destOffset + 2] = (byte) (min16);
        dest[destOffset + 3] = (byte) (min16 >> 8);
        dest[destOffset + 4] = (byte) (mask);
        dest[destOffset + 5] = (byte) (mask >> 8);
        dest[destOffset + 6] = (byte) (mask >> 16);
        dest[destOffset + 7] = (byte) (mask >> 24);
    }

    // The color optimization function. (Clever code, part 1)
    private int optimizeColorsBlock(int[] block) {

        // determine color distribution
        int blocki = block[0];
        int mu_r, min_r, max_r;
        int mu_g, min_g, max_g;
        int mu_b, min_b, max_b;
        mu_r = min_r = max_r = blocki & 0xff;
        mu_g = min_g = max_g = (blocki >> 8) & 0xff;
        mu_b = min_b = max_b = (blocki >> 16) & 0xff;
        for (int i = 1; i < 16; i++) {
            blocki = block[i];
            int r = blocki & 0xff;
            int g = (blocki >> 8) & 0xff;
            int b = (blocki >> 16) & 0xff;
            mu_r += r;
            mu_g += g;
            mu_b += b;
            min_r = Math.min(min_r, r);
            max_r = Math.max(max_r, r);
            min_g = Math.min(min_g, g);
            max_g = Math.max(max_g, g);
            min_b = Math.min(min_b, b);
            max_b = Math.max(max_b, b);
        }
        mu_r = (mu_r + 8) >> 4;
        mu_g = (mu_g + 8) >> 4;
        mu_b = (mu_b + 8) >> 4;

        // determine covariance matrix
        int[] cov = new int[6];

        for (int i = 0; i < 16; i++) {
            blocki = block[i];
            int r = (blocki & 0xff) - mu_r;
            int g = ((blocki >> 8) & 0xff) - mu_g;
            int b = ((blocki >> 16) & 0xff) - mu_b;

            cov[0] += r * r;
            cov[1] += r * g;
            cov[2] += r * b;
            cov[3] += g * g;
            cov[4] += g * b;
            cov[5] += b * b;
        }

        // convert covariance matrix to float, find principal axis via power
        // iter
        float[] covf = new float[6];
        for (int i = 0; i < 6; i++) {
            covf[i] = cov[i] / 255.0f;
        }

        float vfr = max_r - min_r;
        float vfg = max_g - min_g;
        float vfb = max_b - min_b;

        for (int iter = 0; iter < 4; iter++) {
            float r = vfr * covf[0] + vfg * covf[1] + vfb * covf[2];
            float g = vfr * covf[1] + vfg * covf[3] + vfb * covf[4];
            float b = vfr * covf[2] + vfg * covf[4] + vfb * covf[5];

            vfr = r;
            vfg = g;
            vfb = b;
        }

        double magn = Math.max(Math.max(Math.abs(vfr), Math.abs(vfg)), Math.abs(vfb));

        int v_r, v_g, v_b;
        if (magn < 4.0f) { // too small, default to luminance
            v_r = 299; // JPEG YCbCr luma coefs, scaled by 1000.
            v_g = 587;
            v_b = 114;
        } else {
            magn = 512.0 / magn;
            v_r = (int) (vfr * magn);
            v_g = (int) (vfg * magn);
            v_b = (int) (vfb * magn);
        }

        // Pick colors at extreme points
        int minp = 0;
        int maxp = 0;
        int mind = 0x7fffffff, maxd = -0x7fffffff;
        for (int i = 0; i < 16; i++) {
            blocki = block[i];
            int r = (blocki & 0xff) - mu_r;
            int g = ((blocki >> 8) & 0xff) - mu_g;
            int b = ((blocki >> 16) & 0xff) - mu_b;

            int dot = r * v_r + g * v_g + b * v_b;
            if (dot < mind) {
                mind = dot;
                minp = i;
            }

            if (dot > maxd) {
                maxd = dot;
                maxp = i;
            }
        }
        int blockmax = block[maxp];
        int blockmin = block[minp];
        int pmax16 = as16Bit(blockmax & 0xff, (blockmax >> 8) & 0xff, (blockmax >> 16) & 0xff);
        int pmin16 = as16Bit(blockmin & 0xff, (blockmin >> 8) & 0xff, (blockmin >> 16) & 0xff);
        return pmin16 | (pmax16 << 16);
    }

    private int as16Bit(int r, int g, int b) {
        return (mul8Bit(r, 31) << 11) + (mul8Bit(g, 63) << 5) + mul8Bit(b, 31);
    }

    private void evalColors(int[] color, int c0, int c1) {
        stb__From16Bit(color, 0, c0);
        stb__From16Bit(color, 4, c1);
        stb__Lerp13RGB(color, 8, 0, 4);
        stb__Lerp13RGB(color, 12, 4, 0);
    }

    private void stb__From16Bit(int[] out, int offset, int v) {
        int rv = (v & 0xf800) >> 11;
        int gv = (v & 0x07e0) >> 5;
        int bv = (v & 0x001f) >> 0;
        out[offset] = expand5[rv];
        out[offset + 1] = expand6[gv];
        out[offset + 2] = expand5[bv];
        out[offset + 3] = 0;
    }

    // lerp RGB color
    private void stb__Lerp13RGB(int[] out, int offset, int p1, int p2) {
        out[offset] = lerp13(out[p1 + 0], out[p2 + 0]);
        out[offset + 1] = lerp13(out[p1 + 1], out[p2 + 1]);
        out[offset + 2] = lerp13(out[p1 + 2], out[p2 + 2]);
    }

    // The color matching function
    private int matchColorsBlock(int[] block, int[] color) {
        int dirr = color[0] - color[4];
        int dirg = color[1] - color[5];
        int dirb = color[2] - color[6];

        int stop0 = color[0] * dirr + color[1] * dirg + color[2] * dirb;
        int stop1 = color[4] * dirr + color[5] * dirg + color[6] * dirb;
        int stop2 = color[8] * dirr + color[9] * dirg + color[10] * dirb;
        int stop3 = color[12] * dirr + color[13] * dirg + color[14] * dirb;

        // think of the colors as arranged on a line; project point onto that
        // line, then choose
        // next color out of available ones. we compute the crossover points for
        // "best color in top
        // half"/"best in bottom half" and then the same inside that
        // subinterval.
        //
        // relying on this 1d approximation isn't always optimal in terms of
        // euclidean distance,
        // but it's very close and a lot faster.
        // http://cbloomrants.blogspot.com/2008/12/12-08-08-dxtc-summary.html

        int c0Point = (stop1 + stop3) >> 1;
        int halfPoint = (stop3 + stop2) >> 1;
        int c3Point = (stop2 + stop0) >> 1;

        int mask = 0;
        for (int i = 15; i >= 0; i--) {
            int blocki = block[i];
            int r = blocki & 0xff;
            int g = (blocki >> 8) & 0xff;
            int b = (blocki >> 16) & 0xff;
            int dot = r * dirr + g * dirg + b * dirb;
            mask <<= 2;

            if (dot < halfPoint) {
                mask |= (dot < c0Point) ? 1 : 3;
            } else {
                mask |= (dot < c3Point) ? 2 : 0;
            }
        }

        return mask;
    }

    // The refinement function. (Clever code, part 2)
    // Tries to optimize colors to suit block contents better.
    // (By solving a least squares system via normal equations+Cramer's rule)
    private int refineBlock(int[] block, int mask, int pmax16, int pmin16) {
        int min16, max16;

        if (((mask ^ (mask << 2)) & 0xfffffffc) == 0) // all pixels have the
                                                      // same index?
        {
            // yes, linear system would be singular; solve using optimal
            // single-color match on average color
            int r = 8, g = 8, b = 8;
            for (int i = 0; i < 16; i++) {
                int blocki = block[i];
                r += blocki & 0xff;
                g += (blocki >> 8) & 0xff;
                b += (blocki >> 16) & 0xff;
            }
            r >>= 4;
            g >>= 4;
            b >>= 4;
            max16 = (oMatch5[r][0] << 11) | (oMatch6[g][0] << 5) | oMatch5[b][0];
            min16 = (oMatch5[r][1] << 11) | (oMatch6[g][1] << 5) | oMatch5[b][1];
        } else {
            int akku = 0;
            int At1_r, At1_g, At1_b;
            int At2_r, At2_g, At2_b;
            At1_r = At1_g = At1_b = 0;
            At2_r = At2_g = At2_b = 0;
            int cm = mask;
            for (int i = 0; i < 16; i++, cm >>= 2) {
                int step = cm & 3;
                int w1 = w1Tab[step];
                int blocki = block[i];
                int r = blocki & 0xff;
                int g = (blocki >> 8) & 0xff;
                int b = (blocki >> 16) & 0xff;

                akku += prods[step];
                At1_r += w1 * r;
                At1_g += w1 * g;
                At1_b += w1 * b;
                At2_r += r;
                At2_g += g;
                At2_b += b;
            }

            At2_r = 3 * At2_r - At1_r;
            At2_g = 3 * At2_g - At1_g;
            At2_b = 3 * At2_b - At1_b;

            // extract solutions and decide solvability
            int xx = (akku >> 16);
            int yy = (akku >> 8) & 0xff;
            int xy = akku & 0xff;

            float frb = 3.0f * 31.0f / 255.0f / (xx * yy - xy * xy);
            float fg = frb * 63.0f / 31.0f;

            // solve.
            max16 = sclamp((At1_r * yy - At2_r * xy) * frb + 0.5f, 0, 31) << 11;
            max16 |= sclamp((At1_g * yy - At2_g * xy) * fg + 0.5f, 0, 63) << 5;
            max16 |= sclamp((At1_b * yy - At2_b * xy) * frb + 0.5f, 0, 31) << 0;

            min16 = sclamp((At2_r * xx - At1_r * xy) * frb + 0.5f, 0, 31) << 11;
            min16 |= sclamp((At2_g * xx - At1_g * xy) * fg + 0.5f, 0, 63) << 5;
            min16 |= sclamp((At2_b * xx - At1_b * xy) * frb + 0.5f, 0, 31) << 0;
        }
        return min16 | (max16 << 16);
    }

    private int sclamp(float y, int p0, int p1) {
        int x = (int) y;
        return x < p0 ? p0 : x > p1 ? p1 : x;
    }

}
