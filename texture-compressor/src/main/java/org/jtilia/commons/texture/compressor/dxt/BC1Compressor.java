/*
 * This code is based on https://github.com/nothings/stb
 * Copyright (c) 2017 Sean Barrett
 * Java port copyright (c) 2018 Piotr Piastucki piotr@jtilia.org
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.jtilia.commons.texture.compressor.dxt;

public class BC1Compressor extends DXTCompressor {

    /**
     * Compress a single 4x4 block of RGBA data in row-major order using BC1 algorithm. The data must be padded if needed. Alpha
     * is ignored and opaque compression is applied.
     *
     * @param dst
     *            array to store the compressed data in, at least 8 bytes long
     * @param src
     *            input data, 4x4 block of RGBA (4 bytes) data, should be 64 bytes in total
     * @param highQuality
     *            enable high quality mode, does two refinement steps instead of 1. ~30-40% slower.
     */
    public void compressBlock(byte[] dst, byte[] src, boolean highQuality) {
        if (dst.length < 8) {
            throw new IllegalArgumentException("Output buffer too small");
        }
        if (src.length < 64) {
            throw new IllegalArgumentException("Input buffer too small");
        }
        compressColorBlock(dst, 0, src, highQuality);
    }
}
