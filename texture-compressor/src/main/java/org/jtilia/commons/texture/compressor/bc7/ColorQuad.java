/*
 * This code is based on https://github.com/richgel999/bc7enc16
 * Copyright(c) 2018 Richard Geldreich, Jr.
 * Java port copyright (c) 2018 Piotr Piastucki piotr@jtilia.org
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files(the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and / or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions :
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.jtilia.commons.texture.compressor.bc7;

import static org.jtilia.commons.texture.compressor.bc7.Utils.clampi;

class ColorQuad {
    private final byte m_c[] = new byte[4];

    ColorQuad() {
    }

    ColorQuad(ColorQuad o) {
        m_c[0] = o.m_c[0];
        m_c[1] = o.m_c[1];
        m_c[2] = o.m_c[2];
        m_c[3] = o.m_c[3];
    }

    ColorQuad(byte r, byte g, byte b, byte a) {
        m_c[0] = r;
        m_c[1] = g;
        m_c[2] = b;
        m_c[3] = a;
    }

    void dec(int i) {
        m_c[i]--;
    }

    void inc(int i) {
        m_c[i]++;
    }

    int get(int i) {
        return m_c[i] & 0xFF;
    }

    void set(int i, int value) {
        m_c[i] = (byte) (value & 0xFF);
    }

    void set(int i, byte value) {
        m_c[i] = value;
    }

    void setClamped(int r, int g, int b, int a) {
        m_c[0] = (byte) clampi(r, 0, 255);
        m_c[1] = (byte) clampi(g, 0, 255);
        m_c[2] = (byte) clampi(b, 0, 255);
        m_c[3] = (byte) clampi(a, 0, 255);
    }

    void set(int r, int g, int b, int a) {
        assert ((r | g | b | a) <= 255);
        m_c[0] = (byte) r;
        m_c[1] = (byte) g;
        m_c[2] = (byte) b;
        m_c[3] = (byte) a;
    }

    boolean notequals(ColorQuad pRHS) {
        return (m_c[0] != pRHS.m_c[0]) || (m_c[1] != pRHS.m_c[1]) || (m_c[2] != pRHS.m_c[2]) || (m_c[3] != pRHS.m_c[3]);
    }

    void set(ColorQuad q) {
        m_c[0] = q.m_c[0];
        m_c[1] = q.m_c[1];
        m_c[2] = q.m_c[2];
        m_c[3] = q.m_c[3];
    }
}