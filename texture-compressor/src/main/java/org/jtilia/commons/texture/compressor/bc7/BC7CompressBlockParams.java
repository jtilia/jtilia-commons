/*
 * This code is based on https://github.com/richgel999/bc7enc16
 * Copyright(c) 2018 Richard Geldreich, Jr.
 * Java port copyright (c) 2018 Piotr Piastucki piotr@jtilia.org
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files(the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and / or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions :
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.jtilia.commons.texture.compressor.bc7;

public class BC7CompressBlockParams {

    /**
     * m_max_partitions_mode1 may range from 0 (disables mode 1) to BC7ENC16_MAX_PARTITIONS1. The higher this value, the
     * slower the compressor, but the higher the quality.
     */
    public int maxPartitionsMode1;
    /**
     * m_uber_level may range from 0 to BC7ENC16_MAX_UBER_LEVEL. The higher this value, the slower the compressor, but
     * the higher the quality.
     */
    public int uberLevel;
    /**
     * If m_perceptual is true, colorspace error is computed in YCbCr space, otherwise RGB.
     */
    public final boolean perceptual;
    /**
     * Set m_try_least_squares to false for slightly faster/lower quality compression.
     */
    public boolean tryLeastSquares;
    /**
     * Relative RGBA or YCbCrA weights.
     */
    final int[] weights = new int[4];
    /**
     * When m_mode1_partition_estimation_filterbank, the mode1 partition estimator skips lesser used partition patterns
     * unless they are strongly predicted to be potentially useful. There's a slight loss in quality with this enabled
     * (around .08 dB RGB PSNR or .05 dB Y PSNR), but up to a 11% gain in speed depending on the other settings.
     */
    public boolean m_mode1_partition_estimation_filterbank;
    public boolean opaque;

    public BC7CompressBlockParams(boolean perceptual) {
        maxPartitionsMode1 = BC7Compressor.MAX_PARTITIONS1;
        tryLeastSquares = true;
        m_mode1_partition_estimation_filterbank = true;
        uberLevel = 0;
        this.perceptual = perceptual;
        if (perceptual) {
            weights[0] = 128;
            weights[1] = 64;
            weights[2] = 16;
            weights[3] = 32;
        } else {
            weights[0] = 1;
            weights[1] = 1;
            weights[2] = 1;
            weights[3] = 1;
        }
    }

}