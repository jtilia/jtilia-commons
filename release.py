#! /usr/bin/python3
import subprocess
import re

with open("pom.yaml", "r") as file:
  data = file.read();
  p = re.compile('revision: ((?:\d+\.)*)(\d+)(-\w*)', re.MULTILINE)
  m = p.search(data)
# remove -SNAPSHOT
bumped = data
if len(m.groups()) == 3:
  bumped = p.sub('revision: ' + m.group(1) + str(int(m.group(2))), data)
with open("pom.yaml", "w") as out:
  out.write(bumped)

retcode = subprocess.call('mvn clean install', shell=True)

# bump to next snapshot
if len(m.groups()) == 3:
  replacement = 'revision: ' + m.group(1) + str(int(m.group(2)) + 1) + m.group(3)
else:
  replacement = 'revision: ' + m.group(1) + str(int(m.group(2)) + 1)

bumped = p.sub(replacement, data);

with open("pom.yaml", "w") as out:
  out.write(bumped)

print(m.groups())
